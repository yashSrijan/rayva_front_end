import React from "react";
import createBrowserHistory from "history/createBrowserHistory";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import RayvaTheatreContainer  from "./components/Home/HomeContainer.js"
import RayvaWithoutPasswordContainer  from "./components/Home/HomeWithoutPassword.js"

import SizeContainer from "./components/RoomSizes/SizesContainer.js"
import AVSystemContainer from "./components/AVSystems/AVSystemsContainer.js"
import DesignThemeContainer from "./components/DesignThemes/ThemesContainer.js"
import SeatColorContainer from "./components/SeatColor/SeatColorContainer.js"
import PanoramaContainer from "./components/Panorama/PanoramaContainer.js"
import TotalRayvaSolutionContainer from "./components/TotalRayvaSolution/TotalRayvaSolutionContainer.js"
import CustomerInfoContainer from "./components/CustomerInfo/CustomerInfoContainer.js"

import logger from "redux-logger";
import {Provider} from "react-redux";
import reducers from "./reducers/Reducers";
import {createStore, applyMiddleware} from "redux";
import {loadState, saveState} from "./localstorage";


///////////////////////////////////////////////////////////////////////////////////////////////////reduxstuff->
const persistedState = loadState();
const store = createStore(
    reducers,
    persistedState,
    applyMiddleware(logger)
);
// console.log("state right now is : ", store.getState());
store.subscribe(() => {
    saveState(store.getState());
});
//////////////////////////////////////////////////////////////////////////////////////////////////<-reduxstuff
const history = createBrowserHistory();

export default class App extends React.Component {
    constructor() {
        super();
        this.state = {
        };
    }
    //**************************************************************************************************************
    render() {
        return (
            <Provider store = {store} >
                <Router>
                    <Switch>

                        <Route  exact path={"/:dealer"} history={history} render = { props => <RayvaTheatreContainer  {...props} /> } />

                        <Route path={"/:dealer/Size"} history={history} render = { props => <SizeContainer {...props} /> } />

                        <Route path={"/:dealer/AVSystem"} history={history} render = { props => <AVSystemContainer {...props} /> } />

                        <Route path={"/:dealer/DesignTheme"} history={history} render = { props => <DesignThemeContainer {...props} /> } />

                        <Route path={"/:dealer/SeatColor"} history={history} render = { props => <SeatColorContainer {...props} /> } />

                        <Route path={"/:dealer/Panorama"} history={history} render = { props => <PanoramaContainer {...props} /> } />

                        <Route path={"/:dealer/TotalRayvaSolution"} history={history} render = { props => <TotalRayvaSolutionContainer {...props} /> } />

                        <Route path={"/:dealer/CustomerInfo"} history={history} render = { props => <CustomerInfoContainer {...props} /> } />

                        <Route path={"/"} render = { props => <RayvaWithoutPasswordContainer  {...props} /> } />

                    </Switch>
                </Router>
            </Provider>
        );
    }
}


