import { combineReducers } from 'redux'

import {
    SET_ROOMSIZE,
    SET_NUMOFCHAIRS,
    SET_AVSYSTEM,
    SET_DESIGNTHEME,
    SET_CHAIRTYPE,
    SET_VIRTUALLINK,
    SET_PHONEVIRTUALLINK,
    SET_DESIGNTHEMES,

    SET_PASSWORD,
    SET_DEALER,
    SET_URL,
    SET_CURRENCY,

    SET_ROOMCOST,
    SET_SINGLECHAIRCOST,
    SET_AVSYSTEMCOST,
    SET_DESIGNTHEMECOST,
    //SET_TOTALCOST,

} from "./../action/ActionType.js"


const initialState1 = {
    roomsize: "Large long",
    numofchairs: 10,
    avsystem: "Bronze",
    designtheme: "Shapes",
    virtuallink: "http://vr.rayva.com/Shapes/VW/",
    phonevirtuallink: "http://vr.rayva.com/Shapes/VW/VR",
    chairtype: "Californian",
};

const initialState2 = {
    dealer : {},
    password : false,
    url : "",
    currency : "$"
}

const initialState3 = {
    roomcost : 20000,
    singlechaircost : 2000,
    avsystemcost : 22500,
    designthemecost : 37500,
    totalcost : 80000,
}

const initialState4 = {
    designthemes : {}
}

const selectionReducer = ( state = initialState1, action ) => {
    switch(action.type) {
        case  SET_ROOMSIZE: 
            return {
                ...state,
                roomsize : action.roomsize
            }
        case  SET_NUMOFCHAIRS: 
            return {
                ...state,
                numofchairs : action.numofchairs
            }
        case  SET_AVSYSTEM: 
            return {
                ...state,
                avsystem : action.avsystem
            }
        case  SET_DESIGNTHEME: 
            return {
                ...state,
                designtheme : action.designtheme
            }
        case SET_VIRTUALLINK:
            return {
                ...state,
                virtuallink : action.virtuallink
            }
        case SET_PHONEVIRTUALLINK:
            return {
                ...state,
                phonevirtuallink : action.phonevirtuallink
            }
        case  SET_CHAIRTYPE: 
            return {
                ...state,
                chairtype : action.chairtype
            }
        default :
            return state;
    }
};

const dealerReducer = (state = initialState2, action ) => {
    switch(action.type) {
        case  SET_DEALER : 
            return {
                ...state,
                dealer : action.dealer
            }
        case  SET_PASSWORD : 
            return {
                ...state,
                password : action.password
            }
        case SET_URL :
            return {
                ...state,
                url : action.url
            }
        case SET_CURRENCY :
            return {
                ...state,
                currency : action.currency
            }
        default :
            return state;    
    }
};

const designthemesReducer = (state = initialState4, action) => {
    switch(action.type) {
        case SET_DESIGNTHEMES :
            return {
                designthemes : action.designthemes
            }
        default :
            return state;
    }
}

const costReducer = (state = initialState3, action ) => {
    switch(action.type) {
        case  SET_ROOMCOST : 
            return {
                ...state,
                roomcost : action.roomcost,
                totalcost : state.totalcost - state.roomcost + action.roomcost
            }
        case  SET_SINGLECHAIRCOST : 
            return {
                ...state,
                singlechaircost : action.singlechaircost
            }
        case  SET_AVSYSTEMCOST :
            return {
                ...state,
                avsystemcost :action.avsystemcost,
                totalcost : state.totalcost - state.avsystemcost + action.avsystemcost
            }
        case  SET_DESIGNTHEMECOST :
            return {
                ...state,
                designthemecost :action.designthemecost,
                totalcost : state.totalcost - state.designthemecost + action.designthemecost
            }
        // case  SET_TOTALCOST :
        //     return {
        //         ...state,
        //         totalcost : state.totalcost + action.currentcost
        //     }    
        default :
            return state;    
    }
}

export default combineReducers({
    selectionReducer,
    dealerReducer,
    designthemesReducer,
    costReducer
})