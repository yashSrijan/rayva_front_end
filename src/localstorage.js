export const loadState = () => {
    console.log("************************************************In load state function")
    try{
        const serializedState = sessionStorage.getItem('state');
        if(serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (err) {
        return undefined;
    }
};

export const saveState = (state) => {
    console.log("************************************************In save state function")
    try {
        const serializedState = JSON.stringify(state);
        sessionStorage.setItem('state', serializedState);
    } catch (err) {
        //Ignore logging the error
    }
};