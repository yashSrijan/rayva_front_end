import {
    SET_ROOMSIZE,
    SET_NUMOFCHAIRS,
    SET_AVSYSTEM,
    SET_DESIGNTHEME,
    SET_CHAIRTYPE,
    SET_PASSWORD,
    SET_DEALER,
    SET_URL,
    SET_CURRENCY,
    SET_VIRTUALLINK,
    SET_PHONEVIRTUALLINK,
    SET_DESIGNTHEMES,
    SET_ROOMCOST,
    SET_SINGLECHAIRCOST,
    SET_AVSYSTEMCOST,
    SET_DESIGNTHEMECOST,
    //SET_TOTALCOST,
} from "./ActionType.js"

export function setRoomSize(roomsize) {
    return {
        type: SET_ROOMSIZE,
        roomsize: roomsize
    }
}
export function setNumOfChairs(numofchairs) {
    return {
        type: SET_NUMOFCHAIRS,
        numofchairs: numofchairs
    }
}
export function setAVSystem(avsystem) {
    return {
        type: SET_AVSYSTEM,
        avsystem: avsystem
    }
}
export function setDesignTheme(designtheme) {
    return {
        type: SET_DESIGNTHEME,
        designtheme: designtheme
    }
} 
export function setChairType(chairtype) {
    return {
        type:SET_CHAIRTYPE,
        chairtype: chairtype
    }
}
export function setPassword(password) {
    return {
        type : SET_PASSWORD,
        password : password
    }
}
export function setDealer(dealer) {
    return {
        type : SET_DEALER,
        dealer : dealer
    }
}
export function setUrl(url) {
    return {
        type : SET_URL,
        url : url
    }
}
export function setCurrency(currency) {
    return {
        type : SET_CURRENCY,
        currency : currency
    }
}
export function setVirtualLink(virtuallink) {
    return {
        type : SET_VIRTUALLINK,
        virtuallink : virtuallink
    }
}
export function setPhoneVirtualLink(phonevirtuallink) {
    return {
        type : SET_PHONEVIRTUALLINK,
        phonevirtuallink : phonevirtuallink
    }
}
export function setDesignThemes(designthemes) {
    return {
        type : SET_DESIGNTHEMES,
        designthemes : designthemes
    }
}
/////////////////////////////////////////////////cost setting functions :
export function setRoomCost(roomcost) {
    return {
        type : SET_ROOMCOST,
        roomcost :roomcost
    }
}
export function setSingleChairCost(singlechaircost) {
    return {
        type : SET_SINGLECHAIRCOST,
        singlechaircost :singlechaircost
    }
}
export function setAVSystemCost(avsystemcost) {
    return {
        type : SET_AVSYSTEMCOST,
        avsystemcost :avsystemcost
    }
}
export function setDesignThemeCost(designthemecost) {
    return {
        type : SET_DESIGNTHEMECOST,
        designthemecost :designthemecost
    }
}
// export function setTotalCost(currentcost) {
//     return {
//         type : SET_TOTALCOST,
//         currentcost : currentcost
//     }
// }