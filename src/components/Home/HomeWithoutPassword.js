import React from "react";
import { RayvaTheatre } from "./Home.js";

import {connect} from "react-redux";
import {setPassword, setDealer, setUrl} from "./../../action/Actions.js";

class RayvaWithoutPasswordContainer extends React.Component {

    componentDidMount() {
        console.log("this.props.password : ", this.props.password);
    }
    
    render() {
        return (
            <div>
                <RayvaTheatre {...this.props}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        dealer : state.dealerReducer.dealer,
        password : state.dealerReducer.password,
        url : state.dealerReducer.url
    }
};

const mapDispatchToProps = (dispatch) => ( { 

    setUrl : function(dealerurl) {
        dispatch(setUrl(dealerurl));
    },

    setDealer : function(dealer) {
        dispatch(setDealer(dealer));
    },

    setPassword : function(password) {
        dispatch(setPassword(password));
    }

});

export default connect(mapStateToProps, mapDispatchToProps)(RayvaWithoutPasswordContainer);
