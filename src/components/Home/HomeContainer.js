import React from "react";
import axios from "axios";
import {Error} from "./../Error.js";
import { RayvaTheatre } from "./Home.js";
import {PasswordModal} from "./../PasswordModal.js";
import { LoaderImageComponent } from "../LoaderImageComponent.js";

import {connect} from "react-redux";
import {dealerSearchAPI, passwordAPI} from "../../constants/apiConstants";
import {setPassword, setDealer, setUrl, setCurrency} from "./../../action/Actions.js";


class RayvaTheatreContainer extends React.Component {

    constructor(){
        super();
        this.state = {
            password : "",
            error : false,
            passwordModal : false
        }
        this.submitHandler = this.submitHandler.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this);
    }

    componentDidMount() {
        console.log("this.props.password : ", this.props.password);
        let dealerId = this.props.match.params.dealer
        // if(this.props.url === dealerId) {
        //     console.log("url exists already")
        //     this.setState({ loader : false });
        // } else {
            axios({ 
                url: dealerSearchAPI,
                method: 'get',
                params : { dealerId : dealerId }
            })
            .then( res => {
                let dealer = res.data.dealer;
                console.log("dealer received : ", dealer );
                if(dealer === null) {
                    this.setState({ error : true, errorMsg : "Dealer is Invalid"})
                    return;
                }
                if(this.props.url !== this.props.match.params.dealer) {
                    this.props.history.push("/" + this.props.match.params.dealer)
                    this.props.setPassword(false);
                }
                this.props.setUrl(dealerId);
                this.props.setDealer(dealer);
                console.log("RayvaTheatreContainer : this.props : ", this.props);
                if(dealerId === 'rayva') {
                    this.props.setPassword(true);
                } else {
                    this.setState({ passwordModal : true })
                }
            })
            .catch(err => {
                console.error(err);
            })
        //}
    }
    
    submitHandler (e) {
        e.preventDefault();
        axios({ 
            url: passwordAPI,
            method: 'get',
            params : { password : this.state.password, dealer : this.props.match.params.dealer }
        })
        .then( res => {
            let {passwordStatus} = res.data;
            console.log("passwordStatus : ", passwordStatus );
            if(passwordStatus === false) {
                this.setState({ error : true, errorMsg : "Password is Invalid"})
                return;
            }
            //else password is correct and dispatch the action
            this.props.setPassword(passwordStatus);
        })
        .catch(err => {
            console.error(err);
        })
    }

    handleOptionChange (e) {
        this.setState({ password : e.target.value });
    }

    render() {
        let container;
        if(this.props.password) {
            container = <RayvaTheatre {...this.props}/>
        } else if (this.state.passwordModal) {
            container = (
                <PasswordModal password = {this.props.password} handleOptionChange = {this.handleOptionChange} submitHandler = {this.submitHandler}/>
            );
        } else {
            container = <LoaderImageComponent/>
        }
        if(this.state.error) {
            container = <Error error = { this.state.errorMsg }/>
        }
        return (
            <div>
                {container}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    //console.log(state);
    return {
        dealer : state.dealerReducer.dealer,
        password : state.dealerReducer.password,
        url : state.dealerReducer.url,
    }
};

const mapDispatchToProps = (dispatch) => ( { 

    setUrl : function(dealerurl) {
        dispatch(setUrl(dealerurl));
    },

    setDealer : function(dealer) {
        dispatch(setDealer(dealer));
    },

    setPassword : function(password) {
        dispatch(setPassword(password));
    },

    setCurrency : function(currency) {
        dispatch(setCurrency(currency));
    }

});

export default connect(mapStateToProps, mapDispatchToProps)(RayvaTheatreContainer);
