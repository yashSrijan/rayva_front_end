import React from "react";
import { RightArrow } from '../Arrows/Arrows';
import logo_image from "../../assets/images/rayva_logo.png";
import { LoaderImageComponent } from "../LoaderImageComponent";
import {DealerModal} from "./../DealerModal";

export class RayvaTheatre extends React.Component {

    constructor() {
        super();
        this.state = {
            imageLoaded: false,
            askForDealerName: false,
            dealer: ""
        };
    }

    onNext = () => {
        let dealerId = this.props.match.params.dealer
        if (dealerId){
            this.props.history.push("/" + this.props.url +"/Size");
        } else {
            this.setState({
                askForDealerName: true
            })
        }
    }

    onLoad = () => { this.setState({ imageLoaded: true }) }

    submitHandler (e) {
        if (this.state.dealer === "") {
            return;
        }
        console.log("dealer typed : "+ this.state.dealer);
        this.props.history.push("/" + this.state.dealer);
    }

    handleValueChange (e) {
        this.setState({dealer: e.target.value})
    }

    render() {
        let container = (
            <div className="wrapper1" >
                <div className="top_strip"></div>
                <div className="container">
                    <div className="row">
                        <div className="wrapper">
                            <div className="create_text">
                                <p>CREATE YOUR OWN RAYVA THEATER</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="bottom_strip">
                    <div style = {{width : "100%", position:"absolute", textAlign : "center", bottom:"105%"}}>           
                        <img src={logo_image} alt="logo" style={{ width: "50%", margin : "0 auto",}} />
                    </div>
                    <div className="container">
                        <RightArrow clicked={this.onNext} > {<p> Watch With Your Heart</p>} </RightArrow>
                    </div>
                </div>
            </div> 
        );

        if (this.state.imageLoaded === false) {
            container = <LoaderImageComponent />
        }

        if (this.state.askForDealerName === true) {
            container = <DealerModal dealer = {this.props.dealer} handleValueChange = {this.handleValueChange.bind(this)} submitHandler = {this.submitHandler.bind(this)}/> 
        }

        return (
            <div>
                {container}
                <div className="hidden">
                    <img src={require("../../assets/images/extracted1.jpg")} alt="backgroundimage1" onLoad={this.onLoad} />
                </div>
            </div>
        );
    }

}
