import React from "react";

import ThemePopUp from "./ThemePopUp.js";
import ThreeYellowImage from "../../assets/images/3-ylw.png";
import gold_label from "../../assets/images/limited_edition.png";
import TN_NewDesigns from "../../assets/images/themes/Thumbnails/TN_New Designs.png";

import { LoaderImageComponent } from "../LoaderImageComponent";
import { LeftArrow, Heading, TopArrow } from "../Arrows/Arrows";

export class DesignTheme extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            imageLoaded: false,
            clickedDesigntheme: "sports"
        };
        this.setImage = this.setImage.bind(this);
        // console.log("this.props.designthemes : ", this.props.designthemes);
    }


    onPrev = () => { this.props.history.push("/" + this.props.url + "/AVSystem"); }

    onFirst = () => { this.props.history.push("/" + this.props.url); }
    
    onLoad = () => { this.setState({ imageLoaded: true }); }

    selectClickHandler = () => {
        let {designthemes} = this.props
        let { clickedDesigntheme } = this.state;
        let designthemecost = 0, designthemename = "";

        if( (designthemes.twelfththeme) && (designthemes.twelfththeme.name) && (designthemes.twelfththeme.name.replace(/ /g,"").toLowerCase() === clickedDesigntheme) ) {
            designthemecost = parseInt(designthemes.twelfththeme.price.replace(",", ""), 10 );
            designthemename = designthemes.twelfththeme.name;
        } else {
            designthemecost = parseInt( designthemes[clickedDesigntheme].price.replace(",", "") , 10);
            designthemename = designthemes[clickedDesigntheme].name;
        }
        
        this.props.setDesignTheme(designthemename);
        this.props.setDesignThemeCost(designthemecost);
        this.props.setVirtualLink("http://vr.rayva.com/" + designthemename.replace(/ /g, '') + "/VW/");
        this.props.setPhoneVirtualLink("http://vr.rayva.com/" + designthemename.replace(/ /g, '') + "/VW/VR");

        this.props.history.push("/" + this.props.url +"/SeatColor");
    }

    setImage (clickedDesigntheme) {
        console.log('design theme clicked : ', clickedDesigntheme);
        this.setState({ clickedDesigntheme: clickedDesigntheme });
    }

    render() {

        const {clickedDesigntheme} = this.state;
        const {designthemes} = this.props;
        let subimage, heading, goldimage, description = "Design Theme and Engineering: ";

        if (!designthemes.twelfththeme ||  clickedDesigntheme !== designthemes.twelfththeme.name.replace(/ /g,"").toLowerCase()) {

            subimage = designthemes[clickedDesigntheme].artistname
            ? (
                <div id = "xyz">
                    <img src=  { designthemes.baseurl + designthemes[clickedDesigntheme].artistlogo } alt="By Author"/>
                    <p>By {designthemes[clickedDesigntheme].artistname}</p>
                </div>
            ) : null ;
            
            if(subimage === null) {
                subimage = designthemes[clickedDesigntheme].bottomtext ?  ( <div id = "xyz" style = {{top : "95%"}}> <p style = {{fontFamily : "Effra-Regular", fontSize : "12px"}}>{designthemes[clickedDesigntheme].bottomtext}</p> </div>) : null;
            }

            if( designthemes[clickedDesigntheme].limitededition === "true" ) {
                goldimage = (
                    <div id = "gold_image">
                        <img src= {  gold_label } alt="Limited Edition"/>
                    </div>
                );
                heading = (<div style={{marginLeft : '65px'}}> {designthemes[clickedDesigntheme].name}<br/> <span>{description}{this.props.currency}{designthemes[clickedDesigntheme].price}</span> </div>);
            }
            else {
                goldimage = null;
                heading = (<div> {designthemes[clickedDesigntheme].name}<br/> <span>{description}{this.props.currency}{designthemes[clickedDesigntheme].price}</span> </div>)
            }
        }
        
        let container = (
            <div className="wrapper_page_3">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 page_3_content">
                            <div className="row padding_top padding_none">
                                <h2 className = "select_for_designtheme" > SELECT </h2>
                                <h3 className="select">
                                    <img style = {{left : "30px"}} src={ThreeYellowImage} alt="SELECT YOUR DESIGN THEME" />
                                    <div style = {{position:"absolute", zIndex : "999", left:"30%", top:"20%", color:"#199bbe", fontSize:"14px", width:"240px"}}>
                                        ALL RAYVA SOLUTIONS INCLUDE:
                                        <ul style = {{paddingLeft:"20px"}}>
                                            <li style = {{padding:"2px 0 2px 15px", transform:"scale(1,.85)"}}>Design Theme Components</li>
                                            <li style = {{padding:"2px 0 2px 15px", transform:"scale(1,.85)"}}>Lighting</li>
                                            <li style = {{padding:"2px 0 2px 15px", transform:"scale(1,.85)"}}>Room  Customization and Engineering</li>
                                            <li style = {{padding:"2px 0 2px 15px", transform:"scale(1,.85)"}}>Room Acoustics</li>
                                            <li style = {{padding:"2px 0 2px 15px", transform:"scale(1,.85)"}}>Acoustic Treatments</li>
                                            <li style = {{padding:"2px 0 2px 15px", transform:"scale(1,.85)"}}>Full set of Architectural Plans</li>
                                            <li style = {{padding:"2px 0 2px 15px", transform:"scale(1,.85)"}}>Optional Room Isolation Details</li>
                                        </ul>
                                    </div>
                                </h3>
                                <div className="col-sm-6">
                                    <div className="room_size">
                                        <div className="row">
                                            <div className="col-xs-5"></div>
                                            <div className="col-xs-7">
                                                <div className="top_text">  </div>
                                            </div>
                                        </div>
                                        <div className="bottom_text" >
                                            <h2>
                                                SELECT YOUR DESIGN THEME
                                                <h4 className = "bottom_extra">includes rayva engineering and customized architectural plans</h4>
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6" style = {{zIndex : "20", top : "-10px"}}>
                                    <div className="col-sm-12">
                                        {/***********************************************************************************/}
                                        <div className="col-sm-4">
                                            <div className=" row design_theme_image">
                                                <h4>{designthemes.shapes.name}</h4>
                                                <img src={designthemes.baseurl + designthemes.shapes.thumbnail} alt="Shapes" data-toggle="modal" data-target="#myModal" onClick={ () => this.setImage(designthemes.shapes.name.replace(/ /g,"").toLowerCase())} />
                                            </div>
                                            <div className=" row design_theme_image">
                                                <h4>{designthemes.pools.name}</h4>
                                                <img src={designthemes.baseurl + designthemes.pools.thumbnail} alt="Pools" data-toggle="modal" data-target="#myModal" onClick={ () => this.setImage(designthemes.pools.name.replace(/ /g,"").toLowerCase())} />
                                            </div>
                                            <div className=" row design_theme_image">
                                                <h4>{designthemes.soundscapes.name}</h4>
                                                <img src={designthemes.baseurl + designthemes.soundscapes.thumbnail} alt="Soundscapes" data-toggle="modal" data-target="#myModal" onClick={ () => this.setImage(designthemes.soundscapes.name.replace(/ /g,"").toLowerCase())} />
                                            </div>
                                            <div className=" row design_theme_image">
                                                <h4>{designthemes.pulse.name}</h4>
                                                <img src={designthemes.baseurl + designthemes.pulse.thumbnail} alt="Pulse" data-toggle="modal" data-target="#myModal" onClick={ () => this.setImage(designthemes.pulse.name.replace(/ /g,"").toLowerCase())} />
                                            </div>
                                        </div>
                                        {/***********************************************************************************/}
                                        <div className="col-sm-4 ">
                                            <div className= "row design_theme_image">
                                                <h4>{designthemes.movement.name}</h4>
                                                <img src={designthemes.baseurl + designthemes.movement.thumbnail} alt="Movement"  data-toggle="modal" data-target="#myModal" onClick={ () => this.setImage(designthemes.movement.name.replace(/ /g,"").toLowerCase())}/>
                                            </div>
                                            <div className=" row design_theme_image">
                                                <h4>{designthemes.daughtersofthesea.name}</h4>
                                                <img src={designthemes.baseurl + designthemes.daughtersofthesea.thumbnail} alt="Daughters Of The Sea"  data-toggle="modal" data-target="#myModal" onClick={ () => this.setImage(designthemes.daughtersofthesea.name.replace(/ /g,"").toLowerCase())}/>
                                            </div>
                                            <div className=" row design_theme_image">
                                                <h4>{designthemes.illuminations.name}</h4>
                                                <img src={designthemes.baseurl + designthemes.illuminations.thumbnail} alt="Illuminations"  data-toggle="modal" data-target="#myModal" onClick={ () => this.setImage(designthemes.illuminations.name.replace(/ /g,"").toLowerCase())}/>
                                            </div>
                                            <div className=" row design_theme_image">
                                                <h4>{designthemes.origami.name}</h4>
                                                <img src={designthemes.baseurl + designthemes.origami.thumbnail} alt="Origami"  data-toggle="modal" data-target="#myModal" onClick={ () => this.setImage(designthemes.origami.name.replace(/ /g,"").toLowerCase())}/>
                                            </div>
                                        </div>
                                        {/***********************************************************************************/}
                                        <div className="col-sm-4 ">
                                            <div className=" row design_theme_image">
                                                <h4>{designthemes.sports.name}</h4>
                                                <img src={designthemes.baseurl + designthemes.sports.thumbnail} alt="Sports"  data-toggle="modal" data-target="#myModal" onClick={ () => this.setImage(designthemes.sports.name.replace(/ /g,"").toLowerCase())}/>
                                            </div>
                                            <div className=" row design_theme_image">
                                                <h4>{designthemes.lightedge.name}</h4>
                                                <img src={designthemes.baseurl + designthemes.lightedge.thumbnail} alt="Light Edge"  data-toggle="modal" data-target="#myModal" onClick={ () => this.setImage(designthemes.lightedge.name.replace(/ /g,"").toLowerCase())}/>
                                            </div>
                                            <div className=" row design_theme_image">
                                                <h4>{designthemes.cells.name}</h4>
                                                <img src={designthemes.baseurl + designthemes.cells.thumbnail} alt="Cells"  data-toggle="modal" data-target="#myModal" onClick={ () => this.setImage(designthemes.cells.name.replace(/ /g,"").toLowerCase())}/>
                                            </div>
                                            <div className=" row design_theme_image">
                                            {
                                                designthemes.twelfththeme ? (
                                                    <div>
                                                        <h4>{designthemes.twelfththeme.name}</h4>
                                                        <img src={designthemes.baseurl + designthemes.twelfththeme.thumbnail} alt="New Theme"  data-toggle="modal" data-target="#myModal" onClick={ () => this.setImage(designthemes.twelfththeme.name.replace(/ /g,"").toLowerCase())}/>
                                                    </div>
                                                ) : (
                                                    <div>
                                                        <h4> </h4>
                                                        <img src = {TN_NewDesigns} style = {{borderTop : "4px solid #205b66", borderBottom : "12px solid #205b66", cursor:"default"}} alt="New Designs"/>
                                                    </div>
                                                )
                                            }
                                            </div>
                                        </div>
                                        {/***********************************************************************************/}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="bottom_strips">
                        <LeftArrow clicked={this.onPrev} />
                        <TopArrow clicked={this.onFirst} />
                        <Heading dealerImage = {this.props.dealer.dealerinfo.dealerlogo}/>
                    </div>
                </div>
                <div id="myModal" className="modal fade" role="dialog">
                    <div className="modal-dialog" style={{ width: "500px", border : "1px solid white" }}>
                        <div className="modal-content" id = "outer_image">
                            {
                                designthemes.twelfththeme && designthemes.twelfththeme.name.replace(/ /g,"").toLowerCase() === this.state.clickedDesigntheme ? 
                                (
                                    <div>
                                        <div className="themeheading">
                                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                                            <div> {designthemes.twelfththeme.name}<br/> 
                                            <span>{description}{this.props.currency}{designthemes.twelfththeme.price}</span> </div>
                                        </div>
                                        <div className="modal-body" id = "images_container" style={{backgroundColor : "black", padding : "0px 0px"}}>
                                            <img src={designthemes.baseurl + designthemes.twelfththeme.popup} style={{ "width": "100%" }} alt = "Pop-up"/>
                                            {subimage}
                                        </div>
                                        <div className="themeSelect"  onClick = {this.selectClickHandler}  data-dismiss="modal">
                                            Select
                                        </div>
                                        {goldimage}
                                    </div>
                                ) : (
                                    <ThemePopUp heading = {heading} img_src = {designthemes.baseurl + designthemes[clickedDesigntheme].popup} subimage = {subimage} goldimage = {goldimage} selectClickHandler = {this.selectClickHandler} />
                                )
                            }
                        </div>
                    </div>
                </div>
            </div>
        );

        if (this.state.imageLoaded === false) {
            container = <LoaderImageComponent />
        }

        return (
            <div>
                {container}
                <div className="hidden">
                    <img src={require("../../assets/images/extracted3.jpg")} alt="backgroundimage3" onLoad={this.onLoad} />
                </div>
            </div>
        );
    }
}
