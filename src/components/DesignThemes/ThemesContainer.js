import React from "react";
import axios from "axios";
import {Error} from "./../Error.js";
import { DesignTheme } from "./Themes.js";
import { LoaderImageComponent } from "../LoaderImageComponent.js";

import {connect} from "react-redux";
import {dealerSearchAPI, designthemesAPI} from "../../constants/apiConstants";
import {setPassword, setDealer, setUrl, setDesignTheme, setVirtualLink, setPhoneVirtualLink, setDesignThemeCost, setDesignThemes } from "./../../action/Actions.js";


class DesignThemeContainer extends React.Component {

    constructor() {
        super();
        this.state = {
            loader : true,
            error : false
        }
    }

    fetchDesignThemes() {
        axios({
            url: designthemesAPI,
            method: 'get',
            params : {}
        })
        .then( res => {
            let {designthemes} = res.data.designthemes;
            if(designthemes === null) {
                this.setState({loader : false, error : true})
                return;
            } else {
                this.props.setDesignThemes(designthemes);
            }
            this.setState({ loader : false });
        })
        .catch( err => {
            console.error(err);
        })
    }

    componentDidMount() {
        let dealerId = this.props.match.params.dealer
        // if(this.props.url === dealerId){
        //     console.log("url exists already")
        //     this.setState({ loader : false });
        // } else {
            axios({ 
                url: dealerSearchAPI,
                method: 'get',
                params : { dealerId : dealerId }
            })
            .then( res => {
                let dealer = res.data.dealer;
                console.log("dealer received : ", dealer );
                if(dealer === null) {
                    this.setState({loader : false, error : true})
                    return;
                }
                if(this.props.url !== this.props.match.params.dealer) {
                    this.props.setPassword(false);
                    this.props.history.push("/" + this.props.match.params.dealer)
                    return;
                }
                this.props.setUrl(dealerId);
                this.props.setDealer(dealer);
                console.log("DesignThemeContainer : this.props : ", this.props);
                if(this.props.password) {
                   
                } else {
                    this.props.history.push("/" + this.props.url)
                    return;
                }
                this.fetchDesignThemes();
            })
            .catch(err => {
                console.error(err);
            })
        //}
    }
    
    render() {
        let container = <DesignTheme {...this.props}/>
        if(this.state.loader === true) {
            container = <LoaderImageComponent/>
        }
        if(this.state.error) {
            container = <Error/>
        }
        return (
            <div>
                {container}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        dealer : state.dealerReducer.dealer,
        password : state.dealerReducer.password,
        url : state.dealerReducer.url,
        currency : state.dealerReducer.dealer.currency,
        designthemes : state.designthemesReducer.designthemes,
    }
};

const mapDispatchToProps = (dispatch) => ( {
    setUrl : function(dealerurl) {
        dispatch(setUrl(dealerurl));
    },
    setDealer : function(dealer) {
        dispatch(setDealer(dealer));
    },
    setPassword : function(password) {
        dispatch(setPassword(password));
    },
    setDesignTheme : function(designtheme) {
        dispatch(setDesignTheme(designtheme));
    },
    setVirtualLink : function(virtuallink) {
        dispatch(setVirtualLink(virtuallink));
    },
    setPhoneVirtualLink : function(phonevirtuallink) {
        dispatch(setPhoneVirtualLink(phonevirtuallink));
    },
    setDesignThemes : function(designthemes) {
        dispatch(setDesignThemes(designthemes));
    },
    setDesignThemeCost : function(designthemecost) {
        dispatch(setDesignThemeCost(designthemecost));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(DesignThemeContainer);