import React from "react";

const ThemePopUp = (props) => {
    return (
        <div>
            <div className="themeheading" >
                <button type="button" className="close" data-dismiss="modal">&times;</button>
                {props.heading}
            </div>
            <div className="modal-body" id = "images_container" style={{backgroundColor : "black", padding : "0px 0px"}}>
                <img src={props.img_src} style={{ "width": "100%" }} alt = "Pop-up"/>
                {props.subimage}
            </div>
            <div className="themeSelect"  onClick = {props.selectClickHandler}  data-dismiss="modal">
                Select
            </div>
            {props.goldimage}
        </div>
    );
}

export default ThemePopUp;