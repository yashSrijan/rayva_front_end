import React from "react";
import axios from "axios";

import {formAPI} from "../../constants/apiConstants.js";
import { LoaderImageComponent } from "../LoaderImageComponent.js";
import { LeftArrow, Heading, TopArrow } from "../Arrows/Arrows.js";

export class CustomerInfo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            radio: "",
            "cFname": "",
            "cLname": "",
            "cAddress1": "",
            "cAddress2": "",
            "cCity": "",
            "cState": "",
            "cCountry": "",
            "cPhone": "",
            "cEmail": "",
            imageLoaded: false,
            valid: false,
            thankyoudisplay : "none",
            bgdisplay : "none",
            buttonDisable : false,
            ...this.props.dealer.dealerinfo,
            ...this.props.costs,
            ...this.props.selections,
            
        };
        this.onSubmitHandler = this.onSubmitHandler.bind(this);
    }

    handleOptionChange = (event) => {
        if (event.target.name === "radio") {
            this.setState({ radio: event.target.value })
        }
        else {
            this.setState({ [event.target.name]: event.target.value });
        }
    }

    validateForm = () => {
        let Valid = true;
        let objCopy = Object.assign({}, this.state);
        objCopy.cPhone = objCopy.cPhone.replace(/-/g,'');
        objCopy.phone = objCopy.phone.replace(/-/g,'');

        //var newStr = myStr.replace(/,/g, '-');
        let field;
        for (field in objCopy) {
            if (objCopy.hasOwnProperty(field)) {
                    if (Valid) {
                        switch (field) {
                            case 'cFname':
                                Valid = objCopy[field].match(/^[a-zA-Z ,.'-]+$/i);
                                Valid ? Valid = true : alert('Wrong Client First Name !');
                                break;
                            case 'firstname':
                                Valid = objCopy[field].match(/^[a-z ,.'-]+$/i);
                                Valid ? Valid = true : alert('Wrong Surveyor First Name !');
                                break;
                            case 'cLname':
                                Valid = objCopy[field].match(/^[a-zA-Z ,.'-]+$/i);
                                Valid ? Valid = true : alert('Wrong Client Last Name !');
                                break;
                            case 'lastname':
                                Valid = objCopy[field].match(/^[a-z ,.'-]+$/i);
                                Valid ? Valid = true : alert('Wrong Surveyor Last Name !');
                                break;
                            case 'cEmail':
                                Valid = objCopy[field].match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                                Valid ? Valid = true : alert('Wrong Client Email Address !');
                                break;
                            case 'email':
                                Valid = objCopy[field].match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                                Valid ? Valid = true : alert('Wrong Surveyor Email Address !');
                                break;
                            case 'cPhone':
                                Valid = objCopy[field].match(/((?:\+|00)[17](?: |\-)?|(?:\+|00)[1-9]\d{0,2}(?: |\-)?|(?:\+|00)1\-\d{3}(?: |\-)?)?(0\d|\([0-9]{3}\)|[1-9]{0,3})(?:((?: |\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\-)[0-9]{3}(?: |\-)[0-9]{4})|([0-9]{7}))/i);
                                
                                Valid ? Valid = true : alert('Wrong Client Phone Number !');
                                break;
                            case 'phone':
                                Valid = objCopy[field].match(/((?:\+|00)[17](?: |\-)?|(?:\+|00)[1-9]\d{0,2}(?: |\-)?|(?:\+|00)1\-\d{3}(?: |\-)?)?(0\d|\([0-9]{3}\)|[1-9]{0,3})(?:((?: |\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\-)[0-9]{3}(?: |\-)[0-9]{4})|([0-9]{7}))/i);
                                Valid ? Valid = true : alert('Wrong Surveyor Phone Number !');
                                break;
                            default:
                                break;
                        }
                    }
                    else {
                        break;
                    }
            }
        }
        if (Valid) {
            this.setState({ valid: true, buttonDisable : true });
        }
    }

    async onSubmitHandler(e) {
        e.preventDefault();

        await this.validateForm();    //await keyword makes the function execute completely until it return a result

        if (this.state.valid === true) {
            axios.post(formAPI, { ...this.state })
            .then(res => {
                console.log('form submit : ', res);
                if(res.data.status === "Mail sent !"){
                    this.setState({ thankyoudisplay : "block", bgdisplay : "block", buttonDisable : false });
                } else{
                    alert(res.data.status);
                    this.setState({ buttonDisable : false }); //new
                }
            }).catch(err => {
                console.error(err);
            });
        }
        else {
            console.log("email could not be sent !");
        }
    }

    onPrev = () => { this.props.history.push("/" + this.props.url + "/TotalRayvaSolution"); }

    onFirst = () => { this.props.history.push("/" + this.props.url ); }

    onLoad = () => { this.setState({ imageLoaded: true }) }

    render() {

        const array = ["Contractor", "AV Dealer", "Designer", "Architect", "Consultant", "Other"];

        let container = (
            <div>
                <div className="wrapper_page_design">
                    <div  className = "thankyou_modal fadeIn" style = {{display : this.state.thankyoudisplay}}>
                        <p>Thank You !</p>
                        <button className = "btn btn-primary" onClick = {this.onFirst}>Ok</button>
                    </div>
                    <div className="container">
                        {/*****************************************************************************************************/}
                        <div className="row ">
                            <div className="col-md-12">
                                <div className="head_line_last">
                                    <h3>SOLUTION</h3>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="customer_info_heading">
                                    <h4>TOTAL RAYVA SOLUTION</h4>
                                </div>
                            </div>
                        </div>
                        {/*****************************************************************************************************/}
                        <div className="row">
                            <div className="borderDiv">
                                <div className="innerDiv">
                                    <p>PLEASE CHECK ONE</p>
                                </div>
                                <div className="row">
                                    <div className="col-md-2"></div>
                                    <div className="col-md-8 checkbox" style={{ height: "55px", textAlign: "center" }}>
                                        <ul>
                                            {array.map((customer, i) => <li key={i}><label><input type="radio" name="radio" value={customer} onChange={this.handleOptionChange} />&nbsp; {customer}</label></li>)}
                                        </ul>
                                    </div>
                                    <div className="col-md-2"></div>
                                </div>
                            </div>
                        </div>
                        {/*****************************************************************************************************/}
                        <div className="row customer_form_border">
                            <form onSubmit={this.onSubmitHandler}>
                                <div className="col-md-6">
                                    <div className="customer_form">
                                        <div className="c_head">PROPOSAL GENERATED BY</div>
                                        <p className = "singleFieldStyle">
                                            <input type="text" name="firstname" placeholder="FIRST NAME" value = {this.state["firstname"]} onChange={this.handleOptionChange} required/>
                                            <input type="text" name="lastname" placeholder="LAST NAME" value = {this.state["lastname"]} onChange={this.handleOptionChange} required/>
                                            <input type="text" name="company" placeholder="COMPANY" value = {this.state["company"]} onChange={this.handleOptionChange} required/>
                                            <input type="text" name="address1" placeholder="ADDRESS 1" value = {this.state["address1"]} onChange={this.handleOptionChange} required/>
                                            <input type="text" name="address2" placeholder="ADDRESS 2" value = {this.state["address2"]} onChange={this.handleOptionChange}/>
                                            <input type="text" name="city" placeholder="CITY" value = {this.state["city"]} onChange={this.handleOptionChange} required/>
                                            <input type="text" name="state" placeholder="STATE" value = {this.state["state"]} onChange={this.handleOptionChange} required/>
                                            <input type="text" name="country" placeholder="COUNTRY" value = {this.state["country"]} onChange={this.handleOptionChange} required/>
                                            <input type="text" name="phone" placeholder="PHONE" value = {this.state["phone"]} onChange={this.handleOptionChange} required/>
                                            <input type="text" name="email" placeholder="EMAIL" value = {this.state["email"]} onChange={this.handleOptionChange} required/>
                                        </p>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="customer_form">
                                        <div className="c_head">CLIENT INFORMATION</div>
                                        <p className = "singleFieldStyle">
                                            <input type="text" name="cFname" placeholder="FIRST NAME" onChange={this.handleOptionChange} required/>
                                            <input type="text" name="cLname" placeholder="LAST NAME" onChange={this.handleOptionChange} required/>
                                            <input type="text" name="cAddress1" placeholder="ADDRESS 1" onChange={this.handleOptionChange} required/>
                                            <input type="text" name="cAddress2" placeholder="ADDRESS 2" onChange={this.handleOptionChange}/>
                                            <input type="text" name="cCity" placeholder="CITY" onChange={this.handleOptionChange} required/>
                                            <input type="text" name="cState" placeholder="STATE" onChange={this.handleOptionChange} required/>
                                            <input type="text" name="cCountry" placeholder="COUNTRY"  onChange={this.handleOptionChange} required/>
                                            <input type="text" name="cPhone" placeholder="PHONE" onChange={this.handleOptionChange} required/>
                                            <input type="text" name="cEmail" placeholder="EMAIL" onChange={this.handleOptionChange} required/>
                                        </p>
                                    </div>
                                </div>
                                <div className="submit_form_button">
                                    <button type="submit" disabled = {this.state.buttonDisable} className = {`${this.state.buttonDisable  ? '' : ''}`}><p>{this.state.buttonDisable  ? 'SENDING..' : 'PLEASE SUBMIT YOUR SELECTION'}</p></button>
                                </div>
                            </form>
                        </div>
                        {/*****************************************************************************************************/}
                        <div className="bottom_strips">
                            <LeftArrow clicked={this.onPrev} />
                            <TopArrow clicked={this.onFirst} />
                            <Heading dealerImage = {this.props.dealer.dealerinfo.dealerlogo}/>
                        </div>
                        {/*****************************************************************************************************/}
                    </div>
                </div>
                <div style = {{position : "fixed", left : "0", top : "0", width : "100%", height : "100%", backgroundColor : "#000", zIndex: "99",opacity : "0.7", display : this.state.bgdisplay}}></div>
            </div>
        );

        if (this.state.imageLoaded === false) {
            container = <LoaderImageComponent />
        }

        return (
            <div>
                {container}
                <div className="hidden">
                    <img src={require("../../assets/images/img7.jpg")} alt="backgroundimage" onLoad={this.onLoad} />
                </div>
            </div>
        );
    }


}
