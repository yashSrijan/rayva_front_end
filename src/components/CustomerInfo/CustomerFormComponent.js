import React from "react";

export function CustomerFormComponent(props) {
    const array1 = ["First Name", "Last Name"];
    const array2 = ["Address 1", "Address 2", "City", "State", "Phone", "Email"];
    const prefix = props.prefix;
    return (
        <div className="col-md-6">
            <div className="customer_form">
                <div className="c_head">{props.heading}</div>
                <p>
                    {array1.map((field, i) => <input style = {{ textTransform : "uppercase", transform : "scale(1, .8)", letterSpacing : "2px"}} key={i} type="text" name={prefix + field} placeholder={field} onChange={props.onChange} />)}
                    {props.children}
                    {array2.map((field, i) => <input style = {{textTransform : "uppercase", transform : "scale(1, .8)", letterSpacing : "2px"}} key={i} type="text" name={prefix + field} placeholder={field} onChange={props.onChange} />)}
                </p>
            </div>
        </div>
    );
}