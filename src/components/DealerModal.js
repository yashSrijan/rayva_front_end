import React from "react";

export class DealerModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dealer : ""
        };
    }
    
    render() {
        
        return (
            <div id="myModal" role="dialog" >
                <div className = "modal-dialog">
                    <div className = "modal-content">
                        <div className = "modal-header">
                            <h4 className = "modal-title">Enter Dealer Name</h4>
                        </div>
                        <div className = "modal-body">
                            <form onSubmit={ (e) => this.props.submitHandler(e) }>
                                <div className = "form-group">
                                    <label htmlFor="dealername">Dealer Name :</label>
                                    <input required className = "form-control" id="dealerName" onChange={ (e) => this.props.handleValueChange(e)}/>
                                </div>
                                <button type="submit" className = "btn btn-default">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}
