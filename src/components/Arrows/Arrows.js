import React from "react";

import LeftButton from '../../assets/images/left_button.png';
import RightButton from '../../assets/images/right_button.png';
import TopButton from '../../assets/images/top_button.png'
import rayva_logo_image from "../../assets/images/rayva_base_logo.png";

export function LeftArrow(props) {
    return (
        <div className="bottom_strip_left">
            <span><img src={LeftButton} alt="Left Arrow" onClick={props.clicked} /></span>
        </div>
    )
}
export function TopArrow(props) {
    return (
        <div className="bottom_strip_top"  onClick={props.clicked} >
            <span><img src={TopButton} alt="Top Arrow"/><span className = "text">GO TO FIRST PAGE</span></span>
        </div>
    )
}

export function RightArrow(props) {
    return (
        <div className="bottom_strip_content">
            {props.children}
            <span><img src={RightButton} alt="Right Arrow" onClick={props.clicked} /></span>
        </div>
    )
}

export function Heading(props) {
    return (
        <div className="heading">
            {
                props.dealerImage && 
                <img src = {props.dealerImage} alt="dealer-logo"/>
            }
            <img src = {rayva_logo_image} alt="rayva-logo"/>
        </div>
    )
}
