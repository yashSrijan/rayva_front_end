import React from "react";

export function LoaderImageComponent(props){
    return (
        <div className="loder">
            <h2><img src={require("../assets/images/loading3.gif")} alt="Loader Img" /></h2>
        </div>
    )
}