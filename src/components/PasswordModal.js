import React from "react";

export class PasswordModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            password : ""
        };
    }
    
    render() {
        
        return (
            <div className = {`modal ${this.props.password  ? '' : 'show'}`} id="myModal" role="dialog" >
                <div className = "modal-dialog">
                    <div className = "modal-content">
                        <div className = "modal-header">
                            <h4 className = "modal-title">Enter Password</h4>
                        </div>
                        <div className = "modal-body">
                            <form onSubmit={ (e) => this.props.submitHandler(e) }>
                                <div className = "form-group">
                                    <label htmlFor="password">Password :</label>
                                    <input required type="password" className = "form-control" id="password" onChange={ (e) => this.props.handleOptionChange(e)}/>
                                </div>
                                <button type="submit" className = "btn btn-default">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}
