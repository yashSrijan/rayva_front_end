import React from "react";
import { SingleFieldComponent } from "./SingleFieldComponent";
import { LeftArrow, RightArrow, Heading, TopArrow } from "../Arrows/Arrows";

export class TotalRayvaSolution extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
            Large: 			    "2.png"  ,
			Medium : 		    "5.png"  ,
		    Small : 		    "8.png"  ,
			"X-Small" : 		"11.png" ,
		    "Large long" : 	    "1.png"  ,
			"Medium long" : 	"4.png"  ,
		    "Small long" : 	    "7.png"  ,
		    "X-Small long" : 	"10.png" ,
            "Large short" : 	"3.png"  ,
			"Medium short" : 	"6.png"  ,
			"Small short" : 	"9.png"  ,
            "X-Small short" :   "12.png" ,
			total : ""
        };
	}

	onNext = () => { this.props.history.push("/" + this.props.url +"/CustomerInfo"); }

	onPrev = () => { this.props.history.push("/" + this.props.url + "/Panorama" ); }

	onFirst = () => { this.props.history.push("/" + this.props.url ); }

	render() {
		const {designthemes} = this.props;
		const { numofchairs, roomsize, designtheme, avsystem, chairtype } = this.props;
		const { roomcost, avsystemcost, designthemecost, totalcost, currency } = this.props;
		const designthemeimage = designtheme === designthemes.twelfththeme.name ? designthemes.baseurl + designthemes.twelfththeme.thumbnail : /*require("../../assets/images/themes/Thumbnails/TN_" + designtheme + ".jpg")*/ designthemes.baseurl + designthemes[designtheme.replace(/ /g,"").toLowerCase()].thumbnail;

		return (
			<div className="wrapper_page_design">
				<div className="container">
					<div className="row total_checkout page_3_content">
						<div className="col-md-12">
							<div className="head_line">
								<h2>TOTAL RAYVA SOLUTION</h2>
								<h3>SOLUTION</h3>
							</div>
						</div>
						<div className="col-md-12">
							<div className="total_heading">
								<h4>YOUR SELECTION</h4>
							</div>
						</div>
						<div className="flex_class">
						
							<SingleFieldComponent num = "1" field = "ROOM SIZE" name = {numofchairs + " chairs : "} 
							alt = "RoomSize" src =  { require( "../../assets/images/roomsize/notext/" + this.state[roomsize] ) } 
							cost = {currency + roomcost}>
								<img className="final_section_chair" src={ require( "../../assets/images/" + chairtype + ".png" ) } alt="ChairType" />
							</SingleFieldComponent>

							<SingleFieldComponent num = "2" field = "AV SYSTEM" name = {avsystem + ": "}
							alt = "AVSystem" src = { require("../../assets/images/" + [avsystem] + "_.png") }
							cost = {currency + avsystemcost} />

							<SingleFieldComponent num = "3" field = "DESIGN THEME & ENGINEERING" name = {designtheme + ": "}
							alt = "DesignTheme" src = { designthemeimage }
							cost = {currency + designthemecost} />
							
						</div>
						<div className="col-md-12">
							<div className="total">
								<h4> <span>YOUR TOTAL PRICE: </span>{currency + totalcost} </h4>
								<div className = "circle_total" onClick = {this.onNext}>
									<p style = {{transform : "scale(1, .9)"}}>Click here to complete your selection</p>
								</div>
							</div>
						</div>
						<div className="col-md-12">
							<div className="disclaimer">
								Installation of AV equipment and Design Theme not included in the total price
							</div>
						</div>
					</div>
				</div>
				<div className="bottom_strips" style = {{position : 'relative !important'}}>
					<LeftArrow clicked={this.onPrev}/>
					<TopArrow clicked={this.onFirst}/>
					<Heading dealerImage = {this.props.dealer.dealerinfo.dealerlogo}/>
					<RightArrow clicked={this.onNext}/>
				</div>
			</div>
		);
	}
}