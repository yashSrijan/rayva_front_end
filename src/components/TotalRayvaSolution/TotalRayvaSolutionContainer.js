import React from "react";
import axios from "axios";
import {Error} from "./../Error.js";
import { TotalRayvaSolution } from "./TotalRayvaSolution.js";
import { LoaderImageComponent } from "../LoaderImageComponent.js";

import {connect} from "react-redux";
import {dealerSearchAPI} from "../../constants/apiConstants";
import {setPassword, setDealer, setUrl} from "./../../action/Actions.js";

class TotalRayvaSolutionContainer extends React.Component {

    constructor(){
        super();
        this.state = {
            loader : true,
            error : false
        }
    }

    componentDidMount() {
        let dealerId = this.props.match.params.dealer
        // if(this.props.url === dealerId){
        //     console.log("url exists already")
        //     this.setState({ loader : false });
        // } else {
            axios({ 
                url: dealerSearchAPI,
                method: 'get',
                params : { dealerId : dealerId }
            })
            .then( res => {
                let dealer = res.data.dealer;
                console.log("dealer received : ", dealer );
                if(dealer === null) {
                    this.setState({loader : false, error : true})
                    return;
                }
                if(this.props.url !== this.props.match.params.dealer) {
                    this.props.history.push("/" + this.props.match.params.dealer)
                    this.props.setPassword(false);
                }
                this.props.setUrl(dealerId);
                this.props.setDealer(dealer);
                console.log("TotalRayvaSolutionContainer : this.props : ", this.props);
                if(this.props.password) {
                   
                } else {
                    this.props.history.push("/" + this.props.url)
                }
                this.setState({ loader : false });
            })
            .catch(err => {
                console.error(err);
            })
        //}
    }
    
    render() {
        let container = <TotalRayvaSolution {...this.props}/>
        if(this.state.loader === true) {
            container = <LoaderImageComponent/>
        }
        if(this.state.error) {
            container = <Error/>
        }
        return (
            <div>
                {container}
            </div>
        );
    }
}

function convertToString(num) {
    let str = num.toString();
    let new_str = "";
    let count = 0;
    for (let i = str.length - 1; i >= 0; i--) {
        new_str = new_str + str.charAt(i)
        count++;

        if (count === 3 && i !== 0) {
            new_str = new_str + ',';
            count = 0;
        }
    }
    str = new_str.split("").reverse().join("");
    return str;
}

const mapStateToProps = (state) => {
    //console.log(state);
    return {
        dealer : state.dealerReducer.dealer,
        password : state.dealerReducer.password,
        url : state.dealerReducer.url,
        currency : state.dealerReducer.dealer.currency,
        roomsize : state.selectionReducer.roomsize,
        numofchairs : state.selectionReducer.numofchairs,
        avsystem  : state.selectionReducer.avsystem,
        chairtype : state.selectionReducer.chairtype,
        designtheme : state.selectionReducer.designtheme,
        singlechaircost : convertToString(state.costReducer.singlechaircost),
        avsystemcost : convertToString(state.costReducer.avsystemcost),
        designthemecost : convertToString(state.costReducer.designthemecost),
        roomcost : convertToString(state.costReducer.roomcost),
        totalcost : convertToString(state.costReducer.totalcost),
        designthemes : state.designthemesReducer.designthemes,
    }
};

const mapDispatchToProps = (dispatch) => ( {
    setUrl : function(dealerurl) {
        dispatch(setUrl(dealerurl));
    },
    setDealer : function(dealer) {
        dispatch(setDealer(dealer));
    },
    setPassword : function(password) {
        dispatch(setPassword(password));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(TotalRayvaSolutionContainer);
