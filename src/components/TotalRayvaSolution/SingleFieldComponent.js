import React from "react";

export function SingleFieldComponent(props) {
    //console.log(props) 
    const plus = (props.num === "1" || props.num === "2") ? ( <h4 style = {{position : "absolute", top : "50%", right : "-30px", transform : "translateY(-50%)"}}><span className="glyphicon glyphicon-plus" style = {{color : "yellow", fontSize : "15px"}}></span></h4>) : null;
    return (
        <div className="tamplate" style = {{position : "relative"}}>
            <div className="count">{props.num}</div>
            <div className="title">
                <span style = {{color : "white"}}>{props.field}</span><br />
                {props.name}{props.cost}
            </div>
            <div className="img">
                <img src={props.src} alt={props.alt} />
                {props.children}
            </div>
            {plus}
        </div>
    )

}