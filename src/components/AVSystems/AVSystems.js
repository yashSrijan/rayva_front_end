import React from "react";
//import axios from "axios";
// import {pricingAPI} from "../../constants/apiConstants";
import TwoYellowImage from "../../assets/images/2-ylw.png";
import { LoaderImageComponent } from "../LoaderImageComponent";
import { LeftArrow, Heading, TopArrow } from "../Arrows/Arrows";

export class AVSystem extends React.Component {

    constructor() {
        super();
        this.state = {
            imageLoaded: false,
            bronzeDisplay : "none",
            silverDisplay : "none",
            goldDisplay : "none",
            avsystem_data : {
                "bronze" : {price : "0"},
                "silver" : {price : "0"},
                "gold" : {price : "0"}
            }
        };
    }

    onPrev = () => { this.props.history.push("/" + this.props.url + "/Size"); }

    onFirst = () => { this.props.history.push("/" + this.props.url); }
    
    onLoad = () => { this.setState({ imageLoaded: true }) }

    componentDidMount() {
        let  avsystem_data = {};
        let avsystems = this.props.dealer.categories.avsystem;
        Object.keys(avsystems).forEach( system => {
			avsystem_data[system] = avsystems[system];
		})
		this.setState( { avsystem_data : avsystem_data} );
    }
    
    selectClickHandler = (avsystem) => {
        let avsystemcost = this.state.avsystem_data[avsystem].price;
        avsystemcost = parseInt( avsystemcost.replace(",", "") , 10);

        this.props.setAVSystem(avsystem.charAt(0).toUpperCase() + avsystem.slice(1));
        this.props.setAVSystemCost(avsystemcost);
        
        this.props.history.push("/" + this.props.url +"/DesignTheme");
    }

    BronzeHoverOn = () => {
        this.setState({ bronzeDisplay : "block", silverDisplay : "none", goldDisplay : "none"});
    }
    SilverHoverOn = () => {
        this.setState({ bronzeDisplay : "none", silverDisplay : "block", goldDisplay : "none"});
    }
    GoldHoverOn = () => {
        this.setState({ bronzeDisplay : "none", silverDisplay : "none", goldDisplay : "block"});
    }

    render() {
        const {bronze, silver, gold} = this.props.dealer.categories.avsystem;
        const {avsystem_data, bronzeDisplay, silverDisplay, goldDisplay} = this.state;
        let container = (
            <div className="wrapper_page_3">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 page_3_content">
                            <div className="row padding_top padding_none">
                                <h2 className="select_for_avsystem"> SELECT </h2>
                                <h3 className="select image_two"><img src={TwoYellowImage} alt="" /></h3>
                                
                                <div className="col-sm-4">
                                    <div className="room_size_single">
                                        <div className="row">
                                            <div className="col-xs-5"></div>
                                            <div className="col-xs-7">
                                                
                                            </div>
                                        </div>
                                        <div className="avsystem_selecttext">
                                            <h2>
                                                SELECT YOUR AV SYSTEM
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                                
                                <div className="col-sm-8 avsystem-margin-bottom" style = {{ top:"-5px", position:"relative" }}>
                                    <div className="row">
                                        <div className="col-xs-2" >
                                        </div>
                                        <div className="col-xs-3 bronze_bg" onMouseEnter={this.BronzeHoverOn} > <p>BRONZE</p><h4>{this.props.currency}{avsystem_data["bronze"].price}</h4>
                                        </div>
                                        <div className="col-xs-3 silver_bg" onMouseEnter={this.SilverHoverOn} > <p>SILVER</p><h4>{this.props.currency}{avsystem_data["silver"].price}</h4>
                                        </div>
                                        <div className="col-xs-4 gold_bg" onMouseEnter={this.GoldHoverOn} > <p>GOLD</p><h4>{this.props.currency}{avsystem_data["gold"].price}</h4>
                                        </div>
                                    </div>
                                    <div className="row avsystem_data_row" >
                                        <div className="col-xs-2 avsystem_data_firstcol" >PROJECTOR
                                        </div>
                                        <div className="col-xs-3" >{bronze.projector}
                                        </div>
                                        <div className="col-xs-3" >{silver.projector}
                                        </div>
                                        <div className="col-xs-4" >{gold.projector}
                                        </div>
                                    </div>
                                    <div className="row avsystem_data_row" >
                                        <div className="col-xs-2 avsystem_data_firstcol" >SCREEN
                                        </div>
                                        <div className="col-xs-3" >{bronze.screen}
                                        </div>
                                        <div className="col-xs-3" >{silver.screen}
                                        </div>
                                        <div className="col-xs-4" >{gold.screen}
                                        </div>
                                    </div>
                                    <div className="row avsystem_data_row" >
                                        <div className="col-xs-2 avsystem_data_firstcol" >SPEAKERS
                                        </div>
                                        <div className="col-xs-3" >{bronze.speakers}
                                        </div>
                                        <div className="col-xs-3" >{silver.speakers}
                                        </div>
                                        <div className="col-xs-4" >{gold.speakers}
                                        </div>
                                    </div>
                                    <div className="row avsystem_data_row" >
                                        <div className="col-xs-2 avsystem_data_firstcol" >RECEIVERS, POWER AMPS, PROCESSORS
                                        </div>
                                        <div className="col-xs-3" >{bronze['receivers-power-amps-processors']}
                                        </div>
                                        <div className="col-xs-3" >{silver['receivers-power-amps-processors']}
                                        </div>
                                        <div className="col-xs-4" >{gold['receivers-power-amps-processors']}
                                        </div>
                                    </div>
                                    <div className="row avsystem_data_row" >
                                        <div className="col-xs-2 avsystem_data_firstcol" >AUTOMATION
                                        </div>
                                        <div className="col-xs-3" >{bronze.automation}
                                        </div>
                                        <div className="col-xs-3" >{silver.automation}
                                        </div>
                                        <div className="col-xs-4" >{gold.automation}
                                        </div>
                                    </div>
                                    <div className="row avsystem_data_row" >
                                        <div className="col-xs-2 avsystem_data_firstcol" >ACCESSORIES
                                        </div>
                                        <div className="col-xs-3" >{bronze.accessories}
                                        </div>
                                        <div className="col-xs-3" >{silver.accessories}
                                        </div>
                                        <div className="col-xs-4" >{gold.accessories}
                                        </div>
                                    </div>
                                    {/*
                                    <div className="row avsystem_data_row" >
                                        <div className="col-xs-2 avsystem_data_firstcol" >ROOM ACOUSTICS & DESIGN
                                        </div>
                                        <div className="col-xs-3" >{bronze['room-acoustics-and-design']}
                                        </div>
                                        <div className="col-xs-3" >{silver['room-acoustics-and-design']}
                                        </div>
                                        <div className="col-xs-4" >{gold['room-acoustics-and-design']}
                                        </div>
                                    </div>
                                    <div className="row avsystem_data_row" >
                                        <div className="col-xs-2 avsystem_data_firstcol" >ARCHITECTURAL PLANS
                                        </div>
                                        <div className="col-xs-3" >{bronze['architectural-plans']}
                                        </div>
                                        <div className="col-xs-3" >{silver['architectural-plans']}
                                        </div>
                                        <div className="col-xs-4" >{gold['architectural-plans']}
                                        </div>
                                    </div>
                                    */}
                                    <div className="row avsystem_data_row" >
                                        <div className="col-xs-2 avsystem_data_firstcol" >WARRANTY
                                        </div>
                                        <div className="col-xs-3" >{bronze.warranty}
                                        </div>
                                        <div className="col-xs-3" >{silver.warranty}
                                        </div>
                                        <div className="col-xs-4" >{gold.warranty}
                                        </div>
                                    </div>
                                    <div className="row" style={{ marginTop: "20px" }}>
                                        <div className="col-xs-2" >
                                        </div>
                                        <div className="col-xs-3 bronze_bg_bottom" style = {{ height : "10px" }}>
                                           <div className = "bronze_select" style = {{ display : bronzeDisplay }}  onClick = {() => this.selectClickHandler("bronze")}></div>
                                        </div>
                                        <div className="col-xs-3 silver_bg_bottom" style = {{ height : "10px" }}>
                                            <div className = "silver_select" style = {{ display : silverDisplay }}  onClick = {() => this.selectClickHandler("silver")}></div>
                                        </div>
                                        <div className="col-xs-4 gold_bg_bottom" style = {{ height : "10px" }}>
                                            <div className = "gold_select" style = {{ display : goldDisplay }}  onClick = {() => this.selectClickHandler("gold")}></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="bottom_strips">
                        <LeftArrow clicked={this.onPrev} />
                        <TopArrow clicked={this.onFirst} />
                        <Heading dealerImage = {this.props.dealer.dealerinfo.dealerlogo}/>
                    </div>
                </div>
            </div>
        );

        if (this.state.imageLoaded === false) {
            container = <LoaderImageComponent />
        }

        return (
            <div>
                {container}
                <div className="hidden">
                    <img src={require("../../assets/images/extracted3.jpg")} alt="backgroundimage3" onLoad={this.onLoad} />
                </div>
            </div>
        );
    }


}
