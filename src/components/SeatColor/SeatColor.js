import React from "react";
//import axios from "axios";

import solo from '../../assets/images/Solo.png';
import nero from '../../assets/images/Nero.png';
import metro from '../../assets/images/Metro.png';
import fortuny from '../../assets/images/Fortuny.png';
import gramercy from '../../assets/images/Gramercy.png';
import californian from '../../assets/images/Californian.png';

// import {pricingAPI} from "../../constants/apiConstants";
import { LoaderImageComponent } from "../LoaderImageComponent";
import { LeftArrow, Heading, RightArrow, TopArrow } from "../Arrows/Arrows";

export class SeatColor extends React.Component {

	constructor() {
		super();
		this.state = {
			imageLoaded: false,
			chair_data : {
				"californian" : { price : "0", url : "" },
				"solo": { price : "0", url : "" },
				"metro": { price : "0", url : "" },
				"nero": { price : "0", url : "" },
				"gramercy": { price : "0", url : "" },
				"fortuny": { price : "0", url : "" }
			}
		};
	}


	onPrev = () => { this.props.history.push("/" + this.props.url + "/DesignTheme"); }

	onFirst = () => { this.props.history.push("/" + this.props.url); }

	onNext = () => { this.props.history.push("/" + this.props.url + "/Panorama"); }
	
	onLoad = () => { this.setState({ imageLoaded: true }); }
	
	componentDidMount() {
		let chair_data = {};
		let chairs = this.props.dealer.categories.chair;
		Object.keys(chairs).forEach( chairtype => {
			chair_data[chairtype] = chairs[chairtype];
		})
		this.setState( {chair_data : chair_data} );
	}

	selectClickHandler = (chairtype) => {
		this.props.setChairType(chairtype);
		let singlechaircost = this.state.chair_data[chairtype.toLowerCase()].price;
		singlechaircost = parseInt( singlechaircost.replace(",", "") , 10);
		this.props.setSingleChairCost(singlechaircost);
		this.props.setRoomCost(singlechaircost * this.props.numofchairs);
	}

	
	render() {
		const { chair_data } = this.state;

		let container = (
			<div className="wrapper_page_design">
				<div className="container">
					<div className="row">
						<div className="col-sm-12 page_3_content" >
							<div className="row padding_top padding_none">
								<div className="row">
									{/************************************************************************************************/}
									<div className="col-sm-4" style={{ top: "40px"}}>
										<div className="row">
										</div>
									</div>
									{/************************************************************************************************/}
									<div className="col-sm-8" style={{ top: "40px"}}>
										<div className="row" >
											<div className="col-sm-4">
												<div className="seat_image" style={{ width: "200px" }}>
													<div className = {`seatSelect ${this.props.chairtype ===  'Californian' ? 'selected_chair' : ''}`} onClick={() => this.selectClickHandler("Californian")} title="Californian">SELECT</div>
													<img src={ chair_data["californian"].url ? chair_data["californian"].url : californian } alt="californian" style={{ left: "26px"}}/>
													<h3 className="seat_current_name" >Californian</h3>
													<h3 className="seat_current_cost" >{this.props.currency}{chair_data["californian"].price}</h3>
													<p>SEAT CHOICES</p>
												</div>
											</div>
											<div className="col-sm-4">
												<div className="seat_image" style={{ width: "200px" }}>
													<div className = {`seatSelect ${this.props.chairtype ===  'Solo' ? 'selected_chair' : ''}`} onClick={() => this.selectClickHandler("Solo")} title="Solo">SELECT</div>
													<img src={ chair_data["solo"].url ? chair_data["solo"].url : solo} alt="solo" style={{ left: "26px"}}/>
													<h3 className="seat_current_name" >Solo</h3>
													<h3 className="seat_current_cost" >{this.props.currency}{chair_data["solo"].price}</h3>
												</div>
											</div>
											<div className="col-sm-4">
												<div className="seat_image" style={{ width: "200px" }}>
													<div className = {`seatSelect ${this.props.chairtype ===  'Metro' ? 'selected_chair' : ''}`} onClick={() => this.selectClickHandler("Metro")} title="Metro">SELECT</div>
													<img src={ chair_data["metro"].url ? chair_data["metro"].url : metro} alt="metro" style={{ left: "26px"}} />
													<h3 className="seat_current_name" >Metro</h3>
													<h3 className="seat_current_cost" >{this.props.currency}{chair_data["metro"].price}</h3>
												</div>
											</div>
										</div>
										<div className="row">
											<div className="col-sm-4">
												<div className="seat_image" style={{ width: "200px" }}>
													<div className = {`seatSelect ${this.props.chairtype ===  'Fortuny' ? 'selected_chair' : ''}`} onClick={() => this.selectClickHandler("Fortuny")} title="Fortuny">SELECT</div>
													<img src={ chair_data["fortuny"].url ? chair_data["fortuny"].url : fortuny} alt="fortuny" style={{ left: "26px"}} />
													<h3 className="seat_current_name" >Fortuny</h3>
													<h3 className="seat_current_cost" >{this.props.currency}{chair_data["fortuny"].price}</h3>
												</div>
											</div>
											<div className="col-sm-4">
												<div className="seat_image" style={{ width: "200px" }}>
													<div className = {`seatSelect ${this.props.chairtype ===  'Nero' ? 'selected_chair' : ''}`} onClick={() => this.selectClickHandler("Nero")} title="Nero">SELECT</div>
													<img src={ chair_data["nero"].url ? chair_data["nero"].url : nero} alt="nero" style={{ left: "26px"}} />
													<h3 className="seat_current_name" >Nero</h3>
													<h3 className="seat_current_cost" >{this.props.currency}{chair_data["nero"].price}</h3>
												</div>
											</div>
											<div className="col-sm-4">
												<div className="seat_image" style={{ width: "200px" }}>
													<div className = {`seatSelect ${this.props.chairtype ===  'Gramercy' ? 'selected_chair' : ''}`} onClick={() => this.selectClickHandler("Gramercy")} title="Gramercy">SELECT</div>
													<img src={ chair_data["gramercy"].url ? chair_data["gramercy"].url : gramercy} alt="gramercy" style={{ left: "26px"}} />
													<h3 className="seat_current_name" >Gramercy</h3>
													<h3 className="seat_current_cost" >{this.props.currency}{chair_data["gramercy"].price}</h3>
												</div>
											</div>
										</div>
										<div className="row"><div className="col-sm-12"><div className="seat_footers">All seats available in Black, Grey and Red leather.</div></div></div>
									</div>
									{/************************************************************************************************/}
								</div>
							</div>
						</div>
					</div>
					<div className="bottom_strips">
						<div className="selecttext2_seat">
							SELECT YOUR SEAT
						</div>
						<div className="selecttext_seat">
							SELECT
						</div>
						<LeftArrow clicked={this.onPrev} />
						<Heading dealerImage = {this.props.dealer.dealerinfo.dealerlogo}/>
						<TopArrow clicked={this.onFirst}/>
						<RightArrow clicked={this.onNext} />
					</div>
				</div>
			</div>
		);

		if (this.state.imageLoaded === false) {
			container = <LoaderImageComponent />
		}

		return (
			<div>
				{container}
				<div className="hidden">
					<img src={require("../../assets/images/img7.jpg")} alt="backgroundimage" onLoad={this.onLoad} />
				</div>
			</div>
		);
	}


}
