import React from "react";
import LoaderImage from "../../assets/images/loading3.gif"
export default class SingleSizeComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            imageLoaded: false
        };
    }

    onLoad = () => {
        if(this.state.imageLoaded === false) {
            this.setState({
                imageLoaded: true
            })
        }
    }

    render() {
        const {room_data} = this.props;
        let container;

        if(this.state.imageLoaded === false ) {
            container = (
                <div>
                    <div className="row newheading">
                        {
                            Object.keys(room_data).map(function(property, i) {
                            return <div className="col-sm-4" key = {i}><span>{room_data[property][0]} </span>{property.split(" ")[0].toUpperCase()}<span> {property.split(" ")[1]}</span></div>
                            })
                        }
                    </div>
                    <div className="row newimages">
                        <div className="col-sm-4 text-center">  <img src={ LoaderImage }  alt = "Loading.."/>  </div>
                        <div className = "hidden">
                            {
                                Object.keys(room_data).map(function(property, i) {
                                return <div className="col-sm-4 text-center" key = {i}><img src={room_data[property][1]} alt={property}  data-toggle="modal" data-target="#myModal" onLoad = {this.onLoad} /></div>
                                }.bind(this))
                            }
                        </div>
                    </div>
                </div>
            );
        } else {
            container = (
                <div>
                    <div className="row newheading">
                        {
                            Object.keys(room_data).map(function(property, i) {
                            return <div className="col-sm-4" key = {i}><span>{room_data[property][0]} </span>{property.split(" ")[0].toUpperCase()}<span> {property.split(" ")[1]}</span></div>
                            })
                        }
                    </div>
                    <div className="row newimages">
                        {
                            Object.keys(room_data).map(function(property, i) {
                            return <div className="col-sm-4 text-center" key = {i}><img src={room_data[property][1]} alt={property}  data-toggle="modal" data-target="#myModal" onClick={() => this.props.clicked(property, room_data[property][2])} /></div>
                            }.bind(this))
                        }
                    </div>
                </div>
            );
        }

        return (
            <div>
                {container}
            </div>
        )
    }
}