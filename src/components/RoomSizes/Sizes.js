import React from "react";

import largelong from '../../assets/images/roomsize/new/1.png';
import large from '../../assets/images/roomsize/new/2.png';
import largeshort from '../../assets/images/roomsize/new/3.png';
import mediumlong from '../../assets/images/roomsize/new/4.png';
import medium from '../../assets/images/roomsize/new/5.png';
import mediumshort from '../../assets/images/roomsize/new/6.png';
import smalllong from '../../assets/images/roomsize/new/7.png';
import small from '../../assets/images/roomsize/new/8.png';
import smallshort from '../../assets/images/roomsize/new/9.png';
import xsmalllong from '../../assets/images/roomsize/new/10.png';
import xsmall from '../../assets/images/roomsize/new/11.png';
import xsmallshort from '../../assets/images/roomsize/new/12.png';

import P_largelong from "../../assets/images/roomsize/new/pop_ups/1.png"
import P_large from "../../assets/images/roomsize/new/pop_ups/2.png"
import P_largeshort from "../../assets/images/roomsize/new/pop_ups/3.png"
import P_mediumlong from "../../assets/images/roomsize/new/pop_ups/4.png"
import P_medium from "../../assets/images/roomsize/new/pop_ups/5.png"
import P_mediumshort from "../../assets/images/roomsize/new/pop_ups/6.png"
import P_smalllong from "../../assets/images/roomsize/new/pop_ups/7.png"
import P_small from "../../assets/images/roomsize/new/pop_ups/8.png"
import P_smallshort from "../../assets/images/roomsize/new/pop_ups/9.png"
import P_xsmalllong from "../../assets/images/roomsize/new/pop_ups/10.png"
import P_xsmall from "../../assets/images/roomsize/new/pop_ups/11.png"
import P_xsmallshort from "../../assets/images/roomsize/new/pop_ups/12.png"

import  SingleSizeComponent  from "./SingleSizeComponent"
import OneYellowImage from '../../assets/images/1-ylw.png';
import { LoaderImageComponent } from "../LoaderImageComponent";
import { LeftArrow, Heading, TopArrow } from '../Arrows/Arrows';

export class Size extends React.Component {

    constructor() {
        super();
        this.state = {
            imageLoaded: false,
            room: ' ',
            seats: 0
        };
        this.setImage = this.setImage.bind(this);
    }
    
    onPrev = () => { this.props.history.push("/" + this.props.url); }

    onFirst = () => { this.props.history.push("/" + this.props.url); }

    onLoad = () => { this.setState({ imageLoaded: true }); }

    componentDidMount() {
        //call the api here
    }

    selectClickHandler = () => {
        this.props.setRoomSize(this.state.room);
        this.props.setNumOfChairs(this.state.seats);
        let roomcost = this.state.seats * this.props.singlechaircost;
        this.props.setRoomCost(roomcost);
        
        this.props.history.push("/" + this.props.url +"/AVSystem");
    }

    setImage (property, seats) {
        this.setState({ room: property.trim(), seats : seats });
    }

    render() {

        const image_data = {
            "Large long":    [ "1.", P_largelong     ],
            "Large":         [ "2.", P_large         ],
            "Large short":   [ "3.", P_largeshort    ],

            "Medium long":   [ "4.", P_mediumlong   ],
            "Medium":        [ "5.", P_medium       ],
            "Medium short":  [ "6.", P_mediumshort  ],

            "Small long":    [ "7.", P_smalllong    ],
            "Small":         [ "8.", P_small        ],
            "Small short":   [ "9.", P_smallshort   ],

            "X-Small long":  [ "10.", P_xsmalllong  ],
            "X-Small":       [ "11.", P_xsmall      ],
            "X-Small short": [ "12.", P_xsmallshort ],

            " ": []
        }
        const large_data = {
            "Large long":    ["1.", largelong,    10 ],
            "Large":         ["2.", large,        10 ],
            "Large short":   ["3.", largeshort,   10 ]
        }
        const medium_data = {
            "Medium long":   ["4.", mediumlong,   8 ],
            "Medium":        ["5.", medium,       8 ],
            "Medium short":  ["6.", mediumshort,  4 ]
        }
        const small_data = {
            "Small long":    ["7.", smalllong,    8 ],
            "Small":         ["8.", small,        8 ],
            "Small short":   ["9.", smallshort,   4 ]
        }
        const xsmall_data = {
            "X-Small long":  ["10.", xsmalllong,  6 ],
            "X-Small":       ["11.", xsmall,      6 ],
            "X-Small short": ["12.", xsmallshort, 4 ] 
        }
        const popuproom = this.state.room;

        let container = (
            <div className="wrapper_page_3" style = {{overflow : "hidden"}}>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 page_3_content">
                            <div className="row padding_top padding_none">
                                <h2 className="select"> SELECT </h2>
                                <h3 className="select"><img src={OneYellowImage} alt="One" /></h3>
                                <div className="col-sm-6">
                                    <div className="room_size">
                                        <div className="row" >
                                            <div className="col-xs-5"></div>
                                            <div className="col-xs-7" >
                                                <div className = "row" > <div className="top_text"> <p>SELECT THE ROOM TEMPLATE ON THE RIGHT THAT IS CLOSEST TO THE SIZE OF YOUR ROOM. THE NUMBER OF SEATS IN THE SELECTION WILL BE REFLECTED IN THE ORDER TOTAL.</p> </div></div>
                                                <div className = "row" style  = {{position : "relative"}}>
                                                    <div style = {{width : "100px", height : "100px", backgroundColor : "rgba(1, 59, 71, 0.5)", borderRadius : "50%", position : "absolute", right : "0%", top : "-60px"}}>
                                                        <p style = {{position : "absolute", padding : "10px 15px", fontSize : "13px", transform : "scale(1,.8)", textAlign : "center", color : "#a7a7a7"}}>
                                                            Bar stools are not included in the theater price
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className = "row">
                                            <div className="bottom_text" >
                                                <h2 style={{ width: '80%', top: '-52px' }}>
                                                    SELECT YOUR ROOM SIZE
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="col-sm-2"></div>
                                    <div className="col-sm-8">
                                        {/***********************************************************************************/}
                                        <SingleSizeComponent room_data = {large_data}  clicked = {this.setImage} />
                                        {/***********************************************************************************/}
                                        <SingleSizeComponent room_data = {medium_data} clicked = {this.setImage} />
                                        {/***********************************************************************************/}
                                        <SingleSizeComponent room_data = {small_data}  clicked = {this.setImage} />
                                        {/***********************************************************************************/}
                                        <SingleSizeComponent room_data = {xsmall_data} clicked = {this.setImage} />
                                        {/***********************************************************************************/}
                                    </div>
                                    <div className="col-sm-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="bottom_strips">
                        <LeftArrow clicked={this.onPrev} />
                        <TopArrow clicked={this.onFirst} />
                        <Heading dealerImage = {this.props.dealer.dealerinfo.dealerlogo}/>
                    </div>
                </div>
                <div id="myModal" className="modal fade" role="dialog">
                    <div className="modal-dialog" style={{ width: "274px" }}>
                        <div className="modal-content">
                            <div className="newheading">
                                <button type="button" className="close" data-dismiss="modal">&times;</button>
                                <h4><span>{image_data[popuproom][0]}</span> {popuproom.split(" ")[0].toUpperCase()} <span>{popuproom.split(" ")[1]}</span></h4>
                            </div>
                            <div className="modal-body" style={{ backgroundColor: "black" }}>
                                <img src={image_data[popuproom][1]} style={{ "width": "100%" }} alt="Pop-up" />
                            </div>
                            <div className="roomSelect" onClick={this.selectClickHandler} data-dismiss="modal">
                                Select
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

        if (this.state.imageLoaded === false) {
            container = <LoaderImageComponent />
        }

        return (
            <div>
                {container}
                <div className="hidden">
                    <img src={require("../../assets/images/extracted3.jpg")} alt="backgroundimage3" onLoad={this.onLoad} />
                </div>
            </div>
        );
    }
}
