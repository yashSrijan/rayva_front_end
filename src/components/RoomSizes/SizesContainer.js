import React from "react";
import axios from "axios";
import { Size } from "./Sizes.js";
import {Error} from "./../Error.js"
import { LoaderImageComponent } from "../LoaderImageComponent.js";

import {connect} from "react-redux";
import {dealerSearchAPI} from "../../constants/apiConstants";
import {setPassword, setDealer, setUrl, setRoomSize, setNumOfChairs, setRoomCost } from "./../../action/Actions.js";

class SizeContainer extends React.Component {

    constructor() {
        super();
        this.state = {
            loader : false,
            error : false
        }
    }

    componentDidMount() {
        let dealerId = this.props.match.params.dealer
        // if(this.props.url === dealerId){
        //     console.log("url exists already")
        //     this.setState({ loader : false });
        // } else {
            axios({ 
                url: dealerSearchAPI,
                method: 'get',
                params : { dealerId : dealerId }
            })
            .then( res => {
                let dealer = res.data.dealer;
                console.log("dealer received : ", dealer );
                if(dealer === null) {
                    this.setState({loader : false, error : true})
                    return;
                }
                if(this.props.url !== this.props.match.params.dealer) {
                    this.props.history.push("/" + this.props.match.params.dealer)
                    this.props.setPassword(false);
                }
                this.props.setUrl(dealerId);
                this.props.setDealer(dealer);
                console.log("SizeContainer : this.props : ", this.props);
                if(this.props.password) {
                   
                } else {
                    this.props.history.push("/" + this.props.url)
                }
                this.setState({ loader : false });
            })
            .catch(err => {
                console.error(err);
            })
        //}
    }
    
    render() {
        let container = <Size {...this.props}/>
        if(this.state.loader === true) {
            container = <LoaderImageComponent/>
        }
        if(this.state.error) {
            container = <Error/>
        }
        return (
            <div>
                {container}
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    //console.log(state);
    return {
        dealer : state.dealerReducer.dealer,
        password : state.dealerReducer.password,
        url : state.dealerReducer.url,
        singlechaircost : state.costReducer.singlechaircost,
    }
};

const mapDispatchToProps = (dispatch) => ( {
    setUrl : function(dealerurl) {
        dispatch(setUrl(dealerurl));
    },
    setDealer : function(dealer) {
        dispatch(setDealer(dealer));
    },
    setPassword : function(password) {
        dispatch(setPassword(password));
    },
    setRoomSize : function(roomsize) {
        dispatch(setRoomSize(roomsize));
    },
    setNumOfChairs : function(numofchairs) {
        dispatch(setNumOfChairs(numofchairs));
    },
    setRoomCost : function(roomcost) {
        dispatch(setRoomCost(roomcost));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(SizeContainer);
