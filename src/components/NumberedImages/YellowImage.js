import React from "react";

export function YellowImage(props) {

    //console.log(props);
    const circle = props.class2 ? (<div className={props.class2}> {props.paragraph} </div>) : null;
    const extra = props.h4class ? (<h4 className = {props.h4class}>includes rayva engineering and customized architectural plans</h4>) : null;
    return (
        <div>
            <h2 className={props.h2class}> SELECT </h2>
            <h3 className={props.h3class}><img src={props.src} alt={props.children} /></h3>

            <div className={props.bootclass}>
                <div className={props.class1}>
                    <div className="row">
                        <div className="col-xs-5"></div>
                        <div className="col-xs-7">
                            {circle}
                        </div>
                    </div>
                    <div className="bottom_text" >
                        <h2 style = {props.style_}>
                            {props.children}
                            {extra}
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    )

}