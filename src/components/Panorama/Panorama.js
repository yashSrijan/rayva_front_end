import React from "react";
import axios from "axios";
import { LeftArrow, TopArrow } from '../Arrows/Arrows';
import { LoaderImageComponent } from "../LoaderImageComponent";
import {smsAPI} from "../../constants/apiConstants";

export class Panorama extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            imageLoaded: false
        };
        this.sendLinkHandler = this.sendLinkHandler.bind(this);
    }

    async sendLinkHandler() {
        let {mobileNumber} = this.state;
        let {phonevirtuallink} = this.props;        
        if(mobileNumber.length === 10){
            this.setState( {mobileNumber : "+1" + mobileNumber} );
        } else if(mobileNumber.length === 12 && mobileNumber.charAt(0) === "+" && mobileNumber.charAt(1) === "1"){
            //everything is fine
        } else if(mobileNumber.length === 11 && mobileNumber.charAt(0) === "1") {
            this.setState( {mobileNumber : "+" + mobileNumber} );
        }
        else {
            alert("Please fill correct mobile number !");
            return;
        }

        await this.setState({phonevirtuallink});

        await axios.post(smsAPI, { ...this.state })
        .then( res => {
            if(res.data.msg === "Not sent !"){
                alert(res.data.msg);
            } else{
                alert('Sent !');
            }
        }).catch( err => {
            console.error(err);
        });
    }

    handleOptionChange = (e) => { this.setState({ mobileNumber: e.target.value }); }

    onPrev = () => { this.props.history.push("/" + this.props.url + "/SeatColor"); }

    onFirst = () => { this.props.history.push("/" + this.props.url); }

    onNext = () => { this.props.history.push("/" + this.props.url + "/TotalRayvaSolution"); }

    onLoad = () => { this.setState({ imageLoaded: true }) }

    render() {
        const {virtuallink} = this.props ;

        let container = (
            <div className="wrapper_3d">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6"></div>
                        <div className="col-md-6">
                            <div className="position_relative" style={{height:'100vh'}}>
                                <div className="ravya_3d_tag_line">
                                    <p>RAYVA IN 3D!</p>
                                </div>
                                <div className="circle_with_content">
                                    <h2>3 D</h2>
                                    <p>
                                        <span>
                                            To view your theater in 360 panoramic view please click the link below
                                        </span>
                                        <span>
                                            <a href= {virtuallink}
                                            target = "_blank" rel="noopener noreferrer" className = "btn btn-info" 
                                            style = {{color:"yellow" , margin:"10px 0"}}>{this.props.designtheme}</a>
                                        </span>
                                        <span>
                                            Enter your phone number here <input type = "text" className = "vrLink" onChange={this.handleOptionChange}/> and click <a className = "sendAnchor" onClick = {this.sendLinkHandler} style = {{cursor : "pointer"}}>send</a>. We will send you a link to click and see your design selection in Virtual Reality.
                                        </span>
                                        <span className="select_3d" onClick = {this.onNext} style = {{color:"yellow"}}>
                                            Next
							            </span>
                                    </p>
                                </div>
                                <div className="circle_transparent">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="bottom_strips">
                        <LeftArrow clicked={this.onPrev}/>
                        <TopArrow clicked = {this.onFirst}/>
                    </div>
                </div>
            </div>  
        );

        if(this.state.imageLoaded === false ) {
            container = <LoaderImageComponent/>
        }

        return (
            <div>
                {container}
                <div className="hidden">
                    <img src={require("../../assets/images/rayva_newpage.jpg")} alt="backgroundimage" onLoad={this.onLoad} />
                </div>
            </div>
        );
    }
}