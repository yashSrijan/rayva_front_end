import {baseURL} from './baseURL.js'

export const smsAPI = baseURL + "api/sms";
export const formAPI = baseURL + "api/form";
export const dealerSearchAPI = baseURL + "api/dealer";
export const passwordAPI = baseURL + "api/password";
export const designthemesAPI = baseURL + "api/designthemes"